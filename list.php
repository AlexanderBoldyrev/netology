<?php
	error_reporting(E_ALL);
	mb_internal_encoding('utf-8');
?>
<!DOCTYPE html>
<html>
	<head>
		<link href="style.css" rel="stylesheet">
		<title>Tests download form</title>
		<meta charset="UTF-8">
	</head>
	<body class="action-background">
	<?php
		$currentPath = $_SERVER;
		echo "<pre>";
		var_dump($currentPath);
		echo "<pre>";
		$aploadDir = __DIR__.'/upload/';
		$dir = opendir($aploadDir);
			$countFiles = 0;
			while($file = readdir($dir)){
			    if($file == '.' || $file == '..' || is_dir('upload' . $file)){
			        continue;
			    }
			    $countFiles++;
			}
		closedir($dir);
		if($countFiles !== 0) {
			$testArr = scandir($aploadDir);

			$filelist = array();
			foreach($testArr as $test) {
			    if ($test == '.' || $test == '..') {
			         continue;
			    }
			    $filelist[] = $test;
			}

			if(!empty($filelist)) {
				echo "<ul class='tests-list'>";
					foreach ($filelist as $fileName) {
						$pathTest =  $aploadDir.$fileName;
						$strJSONTest = file_get_contents($pathTest);
						$JSONTest = json_decode($strJSONTest, true);
						if($JSONTest){
							echo "<li><a href='/test.php?testName=$fileName'>Проверить знания: {$JSONTest['name']}</a></li>";
						}
					}

				echo "</ul>";
				echo "или <br>";
				echo "<a href='/'>Перейти на главную</a> </br>";
				echo "<a href='/admin.php'>Загрузить тест</a> <br>";
			} 
		} else {
			echo "<p>Здесь пока еще нет тестов</p>";
			echo "<a href='/'>Перейти на главную</a> </br>";
			echo "<a href='/admin.php'>Загрузить тест</a> <br>";
		}
	?>
	</body>
</html>