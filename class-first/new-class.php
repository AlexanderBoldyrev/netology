<?php
class News {
	public $date;
	//private $title = "";
	//private $content = "";
	public function __construct ($title, $date=false) {

		if($date){
			$this->date = $date;
		}else{
			$this->date = date("d.m.Y");
		}
		$this->title = $title;
	}
	public function setContent($content) {
		$this->content = $content;
	}
	public function __sleep() {
		return array('title', 'date', 'content');
	}
}



$title = "Заголовок новости";
$content1 = "Это контент для новости";

$new1 = new News($title);
$new1->setContent($content1);

serialize($new1);

var_dump($new1);



