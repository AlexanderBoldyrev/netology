<?php
class FlashCart {
	private $manufacturer;
	private $memory;
	private $price=0;
	private $filled=0;
	public function __construct($manufacturer, $memory) {
		$this->manufacturer = $manufacturer;
		$this->memory = $memory;
	}
	public function setPrice($price)
	{
		$this->price = $price;
	}

	public function getPrice()
	{
		return $this->price = $price;
	}

	public function setFilled ($filled) {
		if($filled > $this->memory) {
			echo "Количество заполненнной памяти не может превышать объем флеш карты";
			return;
		}

		$this->filled = $filled;
	}

	public function getRealMenory() {
		return $this->memory - $this->filled;
	}
}

//можно переписать алгоритм заполненности в зависимости от процентов, также предумотреть
//перевод гигбайтов в байты, вычисления погрешности объема (я в разбираюсь как они вычисляются).
// Этот алгоритм можно добавить в функцию getRealMenory.

class Sun {
	public function sunRise () {
		return date_sunrise(time());
	}
	public function sunSet () {
		return date_sunset(time());
	}
}


$sun = new Sun;

var_dump($sun->sunSet());

echo "<br>";


class Flower {
	private $name;
	private $color;
	private $place;
	public function __construct($name, $color, $place) {
		$this->name = $name;
		$this->color = $color;
		$this->place = $place;
	}

	public function getFlower() {
		return array('name' => $this->name, 'color' => $this->color, 'place'=>$this->place);
	}
}

$flower = new Flower("Ромашка", "Белый", "Россия");
var_dump($flower);

echo "<br>";

class BottleOfWater {
	private $volume;
	private $fullness=0;
	public function __construct($volume) {
		$this->volume = $volume;
	}

	//установим его в процентах

	public function setfullness($fullness) {
		if($fullness<0 || $fullness>100){
			echo "Объем устанавливается в процентах и не может быть меньше 0 или быть больше 100";
			return;
		}
		$this->fullness = $this->volume/100*$fullness;
	}

	public function getVolume() {
		return $this->volume;
	}

	public function getfullness() {
		return $this->fullness;
	}

	public function isempty() {
		return $this->fullness == 0;
	}
}

$bottle = new BottleOfWater(500);
$bottle->setfullness(35);

var_dump($bottle->getVolume());
var_dump($bottle -> isempty());
var_dump($bottle -> getfullness());

echo "<br>";

class Window {
	private $size;
	private $shaded;
	public function __construct($size, $shaded=false) {
		$this->size = $size;
		$this->shaded = $shaded;
	}

	public function getWindow() {
		return array('size' => $this->size, 'shaded' => $this->shaded);
	}
}

$window = new Window("50 X 20", false);

var_dump($window->getWindow());