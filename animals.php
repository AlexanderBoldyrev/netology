<?php

$continents = array(
	'Africa' => array('panthera leo', 'rhinocerotidae', 'loxodonta africana', 'panthera pardus', 'lemuridae'),
	'Australia' => array('lupus dingo', 'wallabia', 'cacatua galerita', 'casuarius', 'ptilonorhynchidae'),
	'Antarctica' => array('spheniscidae', 'diomedea epomophora', 'macronectes giganteus', 'phocidae', 'balaenoptera musculus'),
	'Eurasia' =>  array('mustela lutreola', 'meles meles', 'mustela erminea', 'lutra lutra', 'ursus arctos'),
	'North America' => array('ursus americanus', 'enhydra lutris', 'mustela', 'mephitidae', 'panthera onca'),
	'South America' => array('Vicugna pacos', 'inia geoffrensis', 'lama glama', 'nasua', 'vicugna')
	);


$oneName = array();
foreach ($continents as $continent => $animals) {
	foreach ($animals as $animal) {
		if (strripos($animal, ' ')){
			$twoNameArr = explode(' ', $animal);
			foreach ($twoNameArr as $value) {
				array_push($oneName, array($continent, $value));
			}
		}elseif(!strripos($animal, ' ')){
			array_push($oneName, array($continent, $animal));
		}
	}	
}


shuffle($oneName);

$newAnimalsParts = array_chunk($oneName, 2);

$arrNewAnimals = array();

foreach ($newAnimalsParts as $animal) {
	$country = $animal[0][0];
	if(isset($animal[1][1]))
	{
		$animalNewName = $animal[0][1] .' '. $animal[1][1];
	} else {
		$animalNewName = $animal[0][1];
	}
	
	array_push($arrNewAnimals, array($country, $animalNewName));
}

$arrKeys = array_keys($continents);

$sortarrNewAnimals = array();

foreach ($arrKeys as $countryName) {

	$arrFilter = array();

	foreach ($arrNewAnimals as $animal) {
		if($animal[0]===$countryName){
			array_push($arrFilter, $animal[1]);
		}
	}
	$sortarrNewAnimals[$countryName] = $arrFilter;
}

echo "<p>Названия из двух слов разделены и объединены с односложными. Всего получилось 49 слов,</br>
поэтому одно животное будет называться одним словом при любых комбинациях.
</p>";
echo "<p>Обновляейте страницу, чтобы почувствовать разницу))</p>";


foreach ($sortarrNewAnimals as $key => $value) {
	echo "<h2>$key</h2>";
	$listNewAnimals = implode(", ", $value);
	echo "<p>{$listNewAnimals};</p>";
}