
<!DOCTYPE html>
<html>
<head>
	<link href="style.css" rel="stylesheet">
	<title>Справочник организаций</title>
</head>
<?php
	$jsonStr = file_get_contents("phonebook.json");
	$json_array = json_decode($jsonStr, true);
?>
<body>
	
	<?php if($json_array===null):?>
		<p>Не валидный json-файл</p>
	<?php else:?>
		<table>
			<tr>
				<th>Наименованик</th>
				<th>Адресс</th>
				<th>Телефон</th>
				<th>График работы</th>
			</tr>
			<?php foreach($json_array["companies"] as $company):?>
				
				<tr>
					<td><?=$company["name"]?></td>
					<td><?=$company["adress"]?></td>
					<td><?=$company["phone"]?></td>
					<td><?=$company["schedule"]?></td>
				</tr>
			<?php endforeach?>
		</table>
	<?php endif?>
	
</body>
</html>