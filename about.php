﻿<!DOCTYPE>

<?php
	$username = 'Александр';
	$age = 22;
	$email = 'don.boldyrev2014@gmail.com';
	$location = 'Кисловодск';
	$about = 'Действующий фронтэндер, начинающий бэкэндер';
?>

<html lang="ru">
	<head>
        <title><?=$username.' - '.$about?></title>
        <meta charset="utf-8">
        <style>
            body {
                font-family: sans-serif;  
            }
            
            dl {
                display: table-row;
            }
            
            dt, dd {
                display: table-cell;
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h1>Страница пользователя Александр</h1>
        <dl>
            <dt>Имя</dt>
            <dd><?=$username?></dd>
        </dl>
        <dl>
            <dt>Возраст</dt>
            <dd><?=$age?></dd>
        </dl>
        <dl>
            <dt>Адрес электронной почты</dt>
            <dd><a href="mailto:<?=$email?>"><?=$email?></a></dd>
        </dl>
        <dl>
            <dt>Город</dt>
            <dd><?=$location?></dd>
        </dl>
        <dl>
            <dt>О себе</dt>
            <dd><?=$about?></dd>
        </dl>
    

	</body>
</html>