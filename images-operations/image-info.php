<?php
	error_reporting(E_ALL);
	mb_internal_encoding('utf-8');



	
	
	$catalog = "img";
	$fileNumber = 1;
	$fileNumberUpload = 0;
	$extantionFile = ".jpg";

	
	
	$info = array();
	if(file_exists ($catalog."/".$fileNumber.$extantionFile)){
		$fp = fopen('image-info.csv', 'w');
		while(file_exists ($catalog."/".$fileNumber.$extantionFile)){
			$fileName = $catalog."/".$fileNumber.$extantionFile;
			$size = stat($fileName)['size'];
			$sizeStr = round($size/1024, 2) . 'Mb';
			$ctime = stat($fileName)['ctime'];
			$ctimeStr = date("Y-m-d H:i:s", $ctime);
			$nameStr = pathinfo($fileName, PATHINFO_BASENAME);
			$namef = pathinfo($fileName, PATHINFO_FILENAME);
			$info[] = array($namef, $nameStr, $sizeStr, $ctimeStr);
			image_resize($fileName, "small-img/".$namef."-small.jpg", 150, false ,100);
			$fileNumber++;	
		}
		foreach ($info as $fields) {
		    fputcsv($fp, $fields);
		}

		fclose($fp);
	}

	//var_dump($info);

function image_resize(
    $source_path, 
    $destination_path, 
    $newwidth,
    $newheight = FALSE, 
    $quality = FALSE // качество для формата jpeg
    ) {

    ini_set("gd.jpeg_ignore_warning", 1); // иначе на некотоых jpeg-файлах не работает
    
    list($oldwidth, $oldheight, $type) = getimagesize($source_path);
    
    switch ($type) {
        case IMAGETYPE_JPEG: $typestr = 'jpeg'; break;
        case IMAGETYPE_GIF: $typestr = 'gif' ;break;
        case IMAGETYPE_PNG: $typestr = 'png'; break;
    }
    $function = "imagecreatefrom$typestr";
    $src_resource = $function($source_path);
    
    if (!$newheight) { $newheight = round($newwidth * $oldheight/$oldwidth); }
    elseif (!$newwidth) { $newwidth = round($newheight * $oldwidth/$oldheight); }
    $destination_resource = imagecreatetruecolor($newwidth,$newheight);
    
    imagecopyresampled($destination_resource, $src_resource, 0, 0, 0, 0, $newwidth, $newheight, $oldwidth, $oldheight);
    
    if ($type = 2) { # jpeg
        imageinterlace($destination_resource, 1); // чересстрочное формирование изображение
        imagejpeg($destination_resource, $destination_path, $quality);      
    }
    else { # gif, png
        $function = "image$typestr";
        $function($destination_resource, $destination_path);
    }
    
    imagedestroy($destination_resource);
    imagedestroy($src_resource);
}
?>

<?php
echo <<<body
 <!DOCTYPE>
 <html lang="ru">
 <head>
        <title>Информация о картинках</title>
        <meta charset="utf-8">
        <style>
        	body {
        		padding-left: 10px;
        		padding-top: 10px;
        	}
        	li {
        		margin-bottom: 10px;
        	}
        </style>
    </head>
    <body>
<form enctype="multipart/form-data" action="" method="POST">
    Отправить этот файл: <input name="userfile" type="file" />
    <input type="submit" value="Send File" />
</form>
body;
if (($handle = fopen("image-info.csv", "r")) !== FALSE) {
	echo "<ul>";
   while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		$srcSmallImg = "small-img/".$data[0]."-small.jpg";
        $href = "img/".$data[1];
	echo "<li>"."Картинка: "."<a href = $href><img src=$srcSmallImg> $data[1]</a>".", создана $data[2], изменена $data[3]."."</li>";
    }
    fclose($handle);
    echo "</ul>";
}
echo "</body>";
echo "</html>";
?>

