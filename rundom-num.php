<!DOCTYPE html>
 <html>
  <head>
     <title>Угадайте число</title>
  </head>

  <body>
    <?php
      
      $buttonText = "Введите число";
      $message = '';

      $url = explode("?", $_SERVER['REQUEST_URI'])[0];

      session_start();

      if(!isset($_SESSION['randomNum'])&&isset($_GET['number']))
      {
        header("Location: $url");
      }elseif(!isset($_SESSION['randomNum']))
      {
        $_SESSION['randomNum'] = rand(1, 10);
      }

      $randomNum = $_SESSION['randomNum'];
      if(isset($_GET['number']))
      {
        $assumption = $_GET['number'];
        if($assumption > $randomNum){
            $message = 'много';
            $buttonText = "Попробовать еще раз";
        }elseif ($assumption < $randomNum){
            $message = 'мало';
            $buttonText = "Попробовать еще раз";
        } else {
            $message = 'вы угадали (перезагрузите страницу для для повтора)';
            $_SESSION = array();
            $buttonText = "Введите число";
        }
      }
    ?>
    <p>Угадайте число от 1 до 10, которое задумал сервер</p>
  	<form aaction="<?=$_SERVER['PHP_SELF'];?>" method="get">
      <p><input type="text" name="number" /></p>
      <p><input type="submit" value="<?=$buttonText?>"/> <button id="givingup" type="button">Сдаюсь!!!</button></p>
    </form>
    <p><?=$message?></p>
    <p id="tooltip" style="display: none">Загадонное число: <?=$randomNum?></p>
  </body>
  <script type="text/javascript">
    document.getElementById('givingup').onclick = function(e) {
      document.getElementById('tooltip').style.display = 'block';
    }
  </script>
</html>  	